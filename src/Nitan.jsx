import React, { useState } from 'react'
import Gunaraj from './Gunaraj'
import { ar1, objM } from './reduce'
import LearnUseState from './LearnUseState'
import Counter from './Component/Counter'
import HideAndShow from './Component/HideAndShow'
import A from './Component/A'
import CheckArray from './Component/CheckArray'
import LearnUseEffect from './Component/LearnUseEffect'
import LearnUseEffect2 from './Component/LearnUseEffect2'
import LearnChildrenProps from './Component/LearnChildrenProps'
import Form1 from './Component/LearnForm/Form1'
import Form2 from './Component/LearnForm/Form2'
import HwForm1 from './Component/LearnForm/HwForm1'
import HwForm2 from './Component/LearnForm/HwForm2'
import HwForm3 from './Component/LearnForm/HwForm3'
import HwForm4 from './Component/LearnForm/HwForm4'
import Form3 from './Component/LearnForm/Form3'
import HitApi1 from './Component/LearnApi/HitApi1'
import ReadAllContact from './Component/LearnApi/ReadAllContact'
import LearnUseRef from './Component/LearnApi/LearnUseRef'
import LearnRout from './Component/LearningRouting/LearnRout'
import NestingRout from './Component/LearningRouting/NestingRout'


const Nitan = () => {

  // let myBestFriend=["nitan","ram","hari"]

  // let age=19
  // // let canWatchMovies= age>=18?"you can watch movies":"you cannot watch movies"
  // // console.log(canWatchMovies)
  // let message= age===16?"your age is 16"
  //             :age===17?"your age is 17"
  //             :age===18?"your age is 18"
  //             :"your age is neither 16,17,18" 
  // console.log(message)

  // let products = [
  //   {
  //     id: 1,
  //     title: "Product 1",
  //     category: "electronics",
  //     price: 5000,
  //     description: "This is description and Product 1",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  //   {
  //     id: 2,
  //     title: "Product 2",
  //     category: "cloths",
  //     price: 2000,
  //     description: "This is description and Product 2",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  //   {
  //     id: 3,
  //     title: "Product 3",
  //     category: "electronics",
  //     price: 3000,
  //     description: "This is description and Product 3",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  // ];


  // let sortedId=products.sort((a,b)=>a.id-b.id)
  // let sortedCategory=products.sort((a,b)=>a.category.length-b.category.length)
  // let sortedPrice=products.sort((a,b)=>a.price-b.price)
  // console.log(sortedId)
  // console.log(sortedCategory)
  // console.log(sortedPrice)


  // console.log(objM(ar1))

  // let [acomp,setAcomp]=useState(true)

//   let a=true
//   let b=false
//   let num=[1,2,a?3:6,...b?[2,3]:[4,5],6]
// console.log(num)
   

    // let [showComponent,setShowComponent]=useState(true)

    

    return (
    <div>
      {/* <p style={{backgroundColor:"lightblue",color:"black",width:"30vh",height:"30vh",fontSize:"15px"}}>
        this is paragraph
    
      </p>
      <button onClick={()=>{
        console.log("button is clicked")
      }}>
        Click Gunaraj
      </button>


{
  myBestFriend.map((value,i)=>{
    return <div key={i}>
      <div>hello</div>
      my best friend is {value}
      </div> //always place key={i} in 1st tag which is the direct child
  })
}


 {products.map((value,i) =>{
  return <div key={i}>title is {value.title} category is {value.category} cost is {value.price}</div>
 })}


{
    age===16?<p>your age is 16</p>
    :age===17?<p>your age is 17</p>
    :age===18?<p>your age is 18</p>
    :<p>your age is neither 16,17,18</p>
}

<LearnUseState></LearnUseState> */}


{/* <Counter></Counter>
<HideAndShow></HideAndShow> */}


{/* {acomp?<A></A>:null}
<button onClick={()=>{
  setAcomp(!acomp)
}}>toogle</button> */}

{/* <LearnUseEffect></LearnUseEffect> */}


{/* {showComponent && <LearnUseEffect2></LearnUseEffect2>}
<button onClick={()=>{
setShowComponent(!showComponent)
}}>hide</button> */}


{/* <LearnChildrenProps name={"gunaraj"} age={20}>
  <div className='bg-red abc'> <p>this is paragraph 1</p>
  <p>this is paragraph 2</p> </div> </LearnChildrenProps> */}

    {/* <Form1></Form1> */}
    
    {/* <Form2></Form2> */}

    {/* <HwForm1></HwForm1>  */}

    {/* <HwForm2></HwForm2> */}

     {/* <HwForm3></HwForm3> */}

    {/* <HwForm4></HwForm4> */}

    {/* <HitApi1></HitApi1> */}

    {/* <Form3></Form3> */}
     
    {/* <ReadAllContact></ReadAllContact> */}

    {/* <LearnUseRef></LearnUseRef> */}

    <LearnRout></LearnRout>

        <NestingRout></NestingRout>
    </div>
  )
}

export default Nitan


// let {age,address,number} = props
{/* <Gunaraj name="Gunaraj" age="20" address="basundhara" phone="9843419078"></Gunaraj> */}
// component will be rendered if state variable changes if state variable if not affected the component will not render
// prop drilling is the problem that doesn't let props to send directly to required component euta component bata arko ma pathauna lai aru child ma pathayerw matrw milxa
// when does useEffect function will run?
// // if dependency exist
// // at first render function will run
// // at second render function will run if any dependency changes
// // if dependency doesnt exists
// //for every render the function will run

// when a component is removed nothing executes expect return code of use effect will execute

// Objects are not valid as a React Child:  This error appears when we call an object in the div.
// Note: execution of setState variable is called always through button click and put inside useEffect () function.

// custom hook 
// it is a function prefixed by use
// the different between normal function and custom hook is we can use inbuilt hook (useState,useEffect,useRef)i hook but we can not use inbuilt hook in function
