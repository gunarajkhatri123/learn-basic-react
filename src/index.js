import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Nitan from './Nitan';
import './learn.css';
import { BrowserRouter } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
  <BrowserRouter>
    <Nitan></Nitan>
    </BrowserRouter>
  // </React.StrictMode>

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
{/* <p>my name is jdj</p>
<h1>my name is jdj</h1>
<h2>my name is jdj</h2>
<h3>my name is jdj</h3>
<h4>my name is jdj</h4>
<img src="./logo512.png" height="100px" width="500px"></img>
<a href="https://www.youtube.com/"  target="_blank">youtube</a> */}
