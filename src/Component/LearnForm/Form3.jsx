import axios from 'axios'
import React, { useState } from 'react'
import { baseUrl } from '../../config/config'


const Form3 = () => {
    let[fullName,setFullName]=useState("")
    let[address,setAddress]=useState("")
    let[phoneNumber,setPhoneNumber]=useState("")
    let[email,setEmail]=useState("")


  return (
   <form action='' onSubmit={ async (e)=>{
    e.preventDefault()
   let data={ fullName,address,phoneNumber,email}
  
   let info = {
      url:`${baseUrl}/contacts`,
      method:"POST",
      data,
   }
   try {
     let result=await axios(info)
     console.log(result.data.message)
    
   } catch (error) {
    console.log(error.message)
   }
  }}>
    <br></br>

    <label htmlFor='fullName'>FullName:</label>
    <input type='text' id='fullName' value={fullName}
    placeholder='enter your fullName' onChange={(e)=>{
      setFullName(e.target.value)
    }}></input>
        <br></br>

        <label htmlFor='address'>Address:</label>
    <input type='text' id='address' value={address}
    placeholder='enter your address' onChange={(e)=>{
      setAddress(e.target.value)
    }}></input>
        <br></br>

        <label htmlFor='phoneNumber'>PhoneNumber:</label>
    <input type='number' id='phoneNumber' value={phoneNumber}
    placeholder='enter your phoneNumber' onChange={(e)=>{
      setPhoneNumber(e.target.value)
    }}></input>
        <br></br>

        <label htmlFor='email'>Email:</label>
    <input type='email' id='email' value={email}
    placeholder='enter your email' onChange={(e)=>{
      setEmail(e.target.value)
    }}></input>
        <br></br>
        <button type='submit'>Send</button>
   </form>
  )
}

export default Form3
