import React, { useState } from 'react'

const HwForm1 = () => {

    let [name,setName]=useState("")
    let [age,setAge]=useState("")
    let [email,setEmail]=useState("")
    let [gender,setGender]=useState("")
  return (
      <form action='' onSubmit={(e)=>{
        e.preventDefault()
       let info={ 
        name:name,
        age:age,
        email:email,
        gender:gender
      }
      console.log(info)
      }}>
        <br></br>

    <label htmlFor='name'>Name:</label>
    <input type='text' id='name' value={name}
    placeholder='enter your name' onChange={(e)=>{
      setName(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor='age'>Age:</label>
    <input type='number' id='age' value={age}
    placeholder='enter your age' onChange={(e)=>{
      setAge(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor='email'>Email:</label>
    <input type='email' id='email' value={email}
    placeholder='enter your email' onChange={(e)=>{
      setEmail(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor='male'>Gender:</label>
    <br></br>
    <label htmlFor='male'>Male:</label>
    <input checked={gender==="male"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="male" id='male' >
    </input>
    <label htmlFor='female'>Female:</label>
    <input checked={gender==="female"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="female" id='female' >
    </input>
    <label htmlFor='others'>Others:</label>
    <input checked={gender==="others"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="others" id='others' ></input>
    
    <br></br>
    <button type="submit">Send</button>

      </form>
      
    
  )
}

export default HwForm1
