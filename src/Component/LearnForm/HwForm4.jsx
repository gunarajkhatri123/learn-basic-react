import React, { useState } from 'react'

const HwForm4 = () => {
    let[productName,setProductName]=useState("")
    let[productPrice,setProductPrice]=useState("")
    let[productQuantity,setProductQuantity]=useState("")
    let[productCategory,setProductCategory]=useState("")
  return (
    <form action='' onSubmit={(e)=>{
        e.preventDefault()
       let info={ 
        productName,
        productPrice,
        productQuantity,
        productCategory
      }
      console.log(info)
      }}>
        <br></br>

    <label htmlFor='productName'>ProductName:</label>
    <input type='text' id='productName' value={productName}
    placeholder='enter your productName' onChange={(e)=>{
      setProductName(e.target.value)
    }}></input>
        <br></br>

        <label htmlFor='productPrice'>ProductPrice:</label>
    <input type='number' id='productPrice' value={productPrice}
    placeholder='enter productPrice' onChange={(e)=>{
      setProductPrice(e.target.value)
    }}></input>
    <br></br>
    
    <label htmlFor=' productQuantity'>ProductQuantity:</label>
    <input type='number' id='productQuantity' value={productQuantity}
    placeholder='enter productQuantity' onChange={(e)=>{
      setProductQuantity(e.target.value)
    }}></input>
    <br></br>

    

    <br></br>
    <button type='submit'>Send</button>
        </form>
      
    
  )
}

export default HwForm4
