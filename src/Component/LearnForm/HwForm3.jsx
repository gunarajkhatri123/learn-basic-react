import React, { useState } from 'react'

const HwForm3 = () => {
    let [email,setEmail]=useState("")
    let [phoneNumber,setPhoneNumber]=useState("")
    let [password,setPassword]=useState("")
    let [isMarried,setIsMarried]=useState("")
    let [clothSize,setClothSize]=useState("")
    let [gender,setGender]=useState("")
    let [comment ,setComment]=useState("")
    let [role,setRole]=useState("")
    let [age,setAge]=useState("")

    let roles = [
        { label: "Select Role", value: "", disabled: true },
        { label: "Admin", value: "admin" },
        { label: "SuperAdmin", value: "superAdmin" },
        { label: "Customer", value: "customer" },
        { label: "DeliveryPerson", value: "deliveryPerson" },
      ];
  return (
    <form action='' onSubmit={(e)=>{
        e.preventDefault()
       let info={ 
        email,
	    phoneNumber,
	    password,
	    isMarried,
	    clothSize,
	    gender,
	    comment,
        role,
        age
      }
      console.log(info)
      }}>
        <br></br>
        
        <label htmlFor='email'>Email</label>
        <input type='email' id='email' value={email} placeholder='enter email' onChange={(e)=>{
            setEmail(e.target.value)
        }}></input>
        <br></br>
        
        <label htmlFor='phoneNumber'>PhoneNumber</label>
        <input type='number' id='phoneNumber' value={phoneNumber} placeholder='enter phoneNumber' onChange={(e)=>{
            setPhoneNumber(e.target.value)
        }}></input>
        <br></br>

        <label htmlFor='password'>Password</label>
        <input type='password' id='password' value={password} placeholder='enter password' onChange={(e)=>{
            setPassword(e.target.value)
        }}></input>
        <br></br>
        
        <label htmlFor="isMarried">isMarried:</label>
        <input type="checkbox" 
        id="isMarried" 
        checked={isMarried} 
        onChange={(e)=>{
        setIsMarried(e.target.checked);
      }} />
      <br></br>

      <label htmlFor='clothSize'>ClothSize</label>
        <input type='number' id='clothSize' value={clothSize} placeholder='enter clothSize' onChange={(e)=>{
            setClothSize(e.target.value)
        }}></input>
        <br></br>

        <label htmlFor='male'>Gender:</label>
    <br></br>
    <label htmlFor='male'>Male:</label>
    <input checked={gender==="male"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="male" id='male' >
    </input>
    <label htmlFor='female'>Female:</label>
    <input checked={gender==="female"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="female" id='female' >
    </input>
    <label htmlFor='others'>Others:</label>
    <input checked={gender==="others"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="others" id='others' ></input>
    <br></br>

    <label htmlFor="comment">Comment:</label>
        <textarea rows={3} cols={10} 
        id="comment" 
        placeholder='enter your comment' 
        value={comment} 
        onChange={(e)=>{
            setComment(e.target.value)
        }}></textarea>
        <br></br>

        <label htmlFor='role'>Role:</label>
      <select id='role' 
      value={role} 
      onChange={(e)=>{
        setRole(e.target.value);
      }}>
        {roles.map((item,i)=>{
            return (
                <option key={i} value={item.value} disabled={item.disabled} > {item.label}</option>
                )
            })}
            </select>
            <br></br>

            <label htmlFor='age'>Age:</label>
        <input type='number' id='age' value={age} placeholder='enter age' onChange={(e)=>{
            setAge(e.target.value)
        }}></input>
        <br></br>
        <button type='submit'>Send</button>
    </form>
  )
}

export default HwForm3
