import React from 'react'

const Form1 = () => {
  return (
    <>
    <form onSubmit={(e)=>{
        e.preventDefault()
        console.log("form is submitted")
    }}>
        <input type="text" placeholder="text"></input>
        <br></br>
        <input type="password" placeholder="password"></input>
        <br></br>
        <input type="email" placeholder="email"></input>
        <br></br>
        <input type="number" placeholder="number"></input>
        <br></br>
        <input type="date" placeholder="date"></input>
        <br></br>
        <input type="file"></input>
        <br></br>
        <button type="submit">Send</button>
    </form>
    </>
  )
}

export default Form1
