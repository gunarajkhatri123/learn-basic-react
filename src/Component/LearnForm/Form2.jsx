import React, { useState } from 'react'

const Form2 = () => {
    let [name,setName]=useState("")
    let [address,setAddress]=useState("")
    let [password,setPassword]=useState("")
    let [description,setDescription]=useState("")
    let [isMarried,setIsMarried]=useState("")
    let [country,setCountry]=useState("")
    let [movies,setMovies]=useState("")
    let [gender,setGender]=useState("")
    

    let countries = [
        { label: "Select Country", value: "", disabled: true },
        { label: "Nepal", value: "nepal" },
        { label: "China", value: "china" },
        { label: "India", value: "india" },
        { label: "America", value: "america" },
      ];

    let FavMovies = [
        { label: "Select Movies", value: "", disabled: true },
        { label: "serendipity ", value: "serendipity" },
        { label: "GodFather", value: "god" },
        { label: "kgf", value: "kgf" },
        { label: "avenger", value: "avenger" },
      ];

      let genders = [
        { label: "Male", value: "male" },
        { label: "Female", value: "female" },
        { label: "Other", value: "other" },
      ];
    return (
    <form action="" onSubmit={(e)=>{
        e.preventDefault()
        let info={
            name:name,     //if key and value are same then we can write only one word
            address:address,
            password:password,
            description:description,
            isMarried:isMarried,
            country:country,
            movies:movies,
            gender:gender,
        }
        console.log(info)
        }}>
            <br></br>

        <label htmlFor="name">Name:</label>
        <input id="name"
        type='text'
        placeholder='enter your name'
        value={name} 
        onChange={(e)=>{
            setName(e.target.value)
        }}></input>
        <br></br>

        <label htmlFor="address">Address:</label>
        <input id="address" 
        type='text' 
        placeholder='enter your address' 
        value={address} 
        onChange={(e)=>{
            setAddress(e.target.value)
        }}></input>
        <br></br>

        <label htmlFor="password">password:</label>
        <input id="password" 
        type='password' 
        placeholder='enter your password' 
        value={password} 
        onChange={(e)=>{
            setPassword(e.target.value)
        }}></input>
        <br></br>

        <label htmlFor="description">description:</label>
        <textarea rows={10} cols={30} 
        id="description" 
        placeholder='enter your description' 
        value={description} 
        onChange={(e)=>{
            setDescription(e.target.value)
        }}></textarea>
        <br></br>

        <label htmlFor="marriage">marriage:</label>
        <input type="checkbox" 
        id="marriage" 
        checked={isMarried} 
        onChange={(e)=>{
        setIsMarried(e.target.checked);
      }} />
      <br></br>

      <label htmlFor='country'>Country:</label>
      <select id='country' 
      value={country} 
      onChange={(e)=>{
        setCountry(e.target.value);
      }}>
        {countries.map((item,i)=>{
            return (
                <option key={i} value={item.value} disabled={item.disabled} > {item.label}</option>
                )
            })}
            </select>
            <br></br>

      <label htmlFor='movies'>Movies:</label>
      <select id='movies' 
      value={movies} 
      onChange={(e)=>{
        setMovies(e.target.value);
      }}>

        {FavMovies.map((item,i)=>{
            return (
                <option key={i} value={item.value} disabled={item.disabled} > {item.label}</option>
                )
            })}
            </select>
            <br></br>

{/* <label htmlFor='male'>Gender</label>
<br></br>

<label htmlFor='male'>Male</label>
<input checked={gender==="male"} 
onChange={(e)=>{
    setGender(e.target.value);
}} 
type="radio" 
value="male" 
id="male"></input>
<label htmlFor='female'>Female</label>
<input checked={gender==="female"}
 onChange={(e)=>{
    setGender(e.target.value);
}} 
type="radio" 
value="female" 
id="female"></input>
<label htmlFor='Other'>other</label>
<input checked={gender==="female"}
onChange={(e)=>{
    setGender(e.target.value);
}} 
type="radio" 
value="other" 
id="other"></input>


<br></br> */}

<label htmlFor='male'>Gender</label>
{genders.map((item,i)=>{
    return(
        <div key={i}>
<label htmlFor={item.value}>{item.label}</label>
<input checked={gender===item.value} 
onChange={(e)=>{
    setGender(e.target.value);
}} 
type="radio" 
value={item.value} 
id={item.value}></input>
        </div>
    )
})}





<br></br>


              <button type="submit">Send</button>
    </form>
  )
}

export default Form2


// all uses value but boxes uses checked => value ko taau ma checked lekhnu parxa checkbox rw radiobox ma
// all uses e.target.value but checkbox uses e.target.checked
// radio box use e.target.value
//important thing in form is all element must have value and onChange