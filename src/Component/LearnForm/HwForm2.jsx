import React, { useState } from 'react'

const HwForm2 = () => {
    let [productName,setProductName] =useState("")
    let [productManagerEmail,setProductManagerEmail] =useState("")
    let [staffEmail,setStaffEmail] =useState("")
    let [password,setPassword] =useState("")
    let [gender,setGender] =useState("")
    let [productManufactureDate,setProductManufactureDate] =useState("")
    let [managerIsMarried,setManagerIsMarried] =useState("")
    let [managerSpouse,setManagerSpouse] =useState("")
    let [productLocation,setProductLocation] =useState("")
    let [productDescription,setProductDescription] =useState("")
    let [productIsAvailable,setProductIsAvailable] =useState("")

    let locations = [
        { label: "Select location", value: "", disabled: true },
        { label: "Kathmandu", value: "kathmandu" },
        { label: "Bhaktapur", value: "bhaktapur" },
        { label: "Jumla", value: "jumla" },
        { label: "Humla", value: "humla" },
      ];
  return (
   <form  action='' onSubmit={(e)=>{
    e.preventDefault()
   let info={ 
    productName:productName,
    productManagerEmail:productManagerEmail,
    staffEmail:staffEmail,
    password:password,
    gender:gender,
    productManufactureDate:productManufactureDate,
    managerIsMarried:managerIsMarried,
    managerSpouse:managerSpouse,
    productLocation:productLocation,
    productDescription:productDescription,
    productIsAvailable:productIsAvailable
  }
  console.log(info)
  }}>
    <br></br>

  <label htmlFor='productName'>ProductName:</label>
    <input type='text' id='productName' value={productName}
    placeholder='enter your productName' onChange={(e)=>{
      setProductName(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor='productManagerEmail'>ProductManagerEmail:</label>
    <input type='email' id='productManagerEmail' value={productManagerEmail}
    placeholder='enter productManagerEmail' onChange={(e)=>{
      setProductManagerEmail(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor='staffEmail'>StaffEmail:</label>
    <input type='email' id='staffEmail' value={staffEmail}
    placeholder='enter staffEmail' onChange={(e)=>{
      setStaffEmail(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor='password'>Password</label>
        <input type='password' id='password' value={password} placeholder='enter password' onChange={(e)=>{
            setPassword(e.target.value)
        }}></input>
        <br></br>

    <label htmlFor='male'>Gender:</label>
    <br></br>
    <label htmlFor='male'>Male:</label>
    <input checked={gender==="male"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="male" id='male' >
    </input>
    <label htmlFor='female'>Female:</label>
    <input checked={gender==="female"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="female" id='female' >
    </input>
    <label htmlFor='others'>Others:</label>
    <input checked={gender==="others"} onChange={(e)=>{
      setGender(e.target.value)
    }} type='radio' value="others" id='others' ></input>
    <br></br>

    <label htmlFor='productManufactureDate'>ProductManufactureDate:</label>
        <input type='date' id='productManufactureDate' value={productManufactureDate} placeholder='enter productManufactureDate' onChange={(e)=>{
            setProductManufactureDate(e.target.value)
        }}></input>
        <br></br>

        <label htmlFor="managerIsMarried">ManagerIsMarried:</label>
        <input type="checkbox" 
        id="managerIsMarried" 
        checked={managerIsMarried} 
        onChange={(e)=>{
        setManagerIsMarried(e.target.checked);
      }} />
      <br></br>

      <label htmlFor='managerSpouse'>ManagerSpouse:</label>
    <input type='text' id='managerSpouse' value={managerSpouse}
    placeholder='enter managerSpouse' onChange={(e)=>{
      setManagerSpouse(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor='productLocation'>ProductLocation:</label>
      <select id='productLocation' 
      value={productLocation} 
      onChange={(e)=>{
        setProductLocation(e.target.value);
      }}>
        {locations.map((item,i)=>{
            return (
                <option key={i} value={item.value} disabled={item.disabled}> {item.label}</option>
                )
            })}
            </select>
            <br></br>

    <label htmlFor="productDescription">ProductDescription:</label>
        <textarea rows={3} cols={10} 
        id="productDescription" 
        placeholder='enter productDescription' 
        value={productDescription} 
        onChange={(e)=>{
            setProductDescription(e.target.value)
        }}></textarea>
        <br></br>

        <label htmlFor="productIsAvailable">ProductIsAvailable:</label>
        <input type="checkbox" 
        id="productIsAvailable" 
        checked={productIsAvailable} 
        onChange={(e)=>{
        setProductIsAvailable(e.target.checked);
      }} />
      <br></br>
      <button type='submit'>Send</button>
   </form>
  )
}

export default HwForm2
