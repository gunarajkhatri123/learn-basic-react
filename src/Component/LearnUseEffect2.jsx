import React, { useEffect, useState } from 'react'

const LearnUseEffect2 = () => {
let [count,setCount]=useState(0)
    useEffect(() => {
        console.log("i am asynchronous useEffect")
        return ()=>{
            console.log("i am return")
        }
    },[count])
    console.log("i am first")
  return (
    <div>
      LearnUseEffect2
      <br></br>
      {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        change count1
      </button>
    </div>
  )
}

export default LearnUseEffect2

// inside use effect
// first return fun executes then other code above return will execute

// return function
// in 1st render it doesn't execute
// from 2nd render it will run

// when a component is removed nothing executes expect return code of use effect will execute

// component did mount   first ko render
// component did update   from 2nd to ..........
// component did unmount  delete