import React from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'

// http://localhost:3000/about?name=gunaraj&age=20&address=basundhara
const About = () => {
  let [searchParams]=useSearchParams()
  let navigate=useNavigate()
  return (
    <div>
      About page
      <br></br>
      name is {searchParams.get("name")}
      <br></br>
      age is {searchParams.get("age")}
      <br></br>
      address is {searchParams.get("address")}
      <br></br>
      <button onClick={()=>{
        navigate("/contact",{replace:true})
      }}>Go to contact page</button>
    </div>
  )
}

export default About
