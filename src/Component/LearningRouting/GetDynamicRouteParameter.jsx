import React from 'react'
import { useParams } from 'react-router-dom'

const GetDynamicRouteParameter = () => {
    let params=useParams()
  return (
    <div>
      Get Dynamic Route Parameter
      <br></br>
      {params.id1}
      <br></br>
      {params.id2}

    </div>
  )
}

export default GetDynamicRouteParameter
