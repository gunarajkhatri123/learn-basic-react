import React from 'react'
import { NavLink, Route, Routes } from 'react-router-dom'
import About from './About'
import Home from './Home'
import GetDynamicRouteParameter from './GetDynamicRouteParameter'
import Error from './Error'


const LearnRout = () => {
  return (
    <div>
      <NavLink to="/" style={{margin:"20px"}}>Home</NavLink>
      <NavLink to="/about" style={{margin:"20px"}}>About</NavLink>
      <NavLink to="/contact" style={{margin:"20px"}}>Contact</NavLink>
      
      <Routes>
        <Route path='/' element={<Home></Home>}></Route>
        <Route path="/about" element={<About></About>}></Route>
        <Route path='/contact' element={<div>Contact us</div>}></Route>
        <Route path="*" element={<Error></Error>}></Route>
        <Route path="/a/a1" element={<div>a1 page</div>}></Route>
        <Route path="/a/:any" element={<div>any page</div>}></Route>
        <Route path="/b/:id1/id/:id2" element={<GetDynamicRouteParameter></GetDynamicRouteParameter>}></Route>
      </Routes>
    </div>
  )
}

export default LearnRout
