import React from 'react'

const CheckArray = () => {
    let count=1   // since variable isn't defined in useState method it isn't rerendered and click button wont execute anything
  return (
    <div>
      count is {count}
      <br></br>
      <button onClick={()=>{
        count=2
      }}>click me</button>
    </div>
  )
}

export default CheckArray
