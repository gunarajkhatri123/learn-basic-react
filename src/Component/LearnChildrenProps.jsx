import React from 'react'

const LearnChildrenProps = ({name,age,children}) => {
  return (
    <div>
      LearnChildrenProps<br></br>
      {name}<br></br>
      {age}<br></br>
      {children}<br></br>
    </div>
  )
}

export default LearnChildrenProps
