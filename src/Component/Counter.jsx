import React, { useState } from 'react'

const Counter = () => {
    let [count2,setCount2]=useState(80)
  return (
    <div>
      {count2}<br></br>
      <button onClick={()=>{
        setCount2(count2+1)
      }}>
        increment
      </button>
      <button onClick={()=>{
        setCount2(count2-1)
      }}>
        decrement
      </button>
      <button onClick={()=>{
        setCount2(80)
      }}>
        reset
      </button>
      <button onClick={()=>{
        setCount2(count2*count2)
      }}>square</button>
      
    </div>
  )
}

export default Counter
