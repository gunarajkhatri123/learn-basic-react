import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { baseUrl } from '../../config/config'

const ReadAllContact = () => {
    let [contacts,setContacts]=useState([])
    let readAllContact=async()=>{
        let info={
            url:`${baseUrl}/contacts`,
            method:"get",
        }
        let result=await axios(info)
        setContacts(result.data.data.results)
    }
    useEffect(()=>{
        readAllContact()
    },[])

    let deleteContact=async(_id)=>{
        let info={
            url:`${baseUrl}/contacts/${_id}`,
            method:"delete",
        }
        let result=await axios(info)

    }
  return (
    <div>      
        {contacts.map((item,i)=>{
            return(
                <div key={i} style={{border:"solid red 3px"}}>
                    <p>Full name:{item.fullName}</p>
                    <p>Email:{item.email}</p>
                    <p>Address:{item.address}</p>
                    <p>Phone Number:{item.phoneNumber}</p>
                    <button onClick={async()=>{
                        await deleteContact(item._id)
                        await readAllContact()
                    }}>Delete</button>
                </div>
            )
        })}
      read all api
    </div>
  )
}

export default ReadAllContact



// Note: execution of setState variable is called always through button click and put inside useEffect () function.