import axios from 'axios'
import React from 'react'

const HitApi1 = () => {
    let hitApi=async()=>{
        let obj={
            url:"https://fake-api-nkzv.onrender.com/api/v1/contacts",
            method:"get",
        }
        let result=await axios(obj)
        console.log(result.data.data.results)
    }
    let postApi=async()=>{
        let obj1={
            url:"https://fake-api-nkzv.onrender.com/api/v1/contacts",
            method:"post",
            data:{ fullName:"ohno",
            address:"gag",
            phoneNumber:9849468999,
            email:"apa425@gmail.com"}
        }
        let result=await axios(obj1)
        console.log(result)
    }
  return (
    <div>
      <button onClick={()=>{
        hitApi()
      }}> Read All Contact</button>
      <button onClick={()=>{
        postApi()
      }}> create new Contact</button>
    </div>
  )
}

export default HitApi1
