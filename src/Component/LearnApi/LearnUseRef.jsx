import React, { useRef } from 'react'

const LearnUseRef = () => {
    let inputRef1=useRef()
    let inputRef2=useRef()
    let inputRef3=useRef()
  return (
    <div>
        {/* <p ref={inputRef1} style={{backgroundColor:"green"}}>p1</p>
        <p ref={inputRef2} style={{backgroundColor:"yellow"}}>p1</p>
      <button onClick={()=>{
        inputRef1.current.style.backgroundColor="red"
        inputRef2.current.style.backgroundColor="blue"
      }}>change bg of p</button>
      <br></br> */}
      <input ref={inputRef3}></input>
      <button onClick={()=>{
        inputRef3.current.focus()
      }}>change focus</button>
    </div>
  )
}

export default LearnUseRef
