import React, { useState } from 'react'

const HideAndShow = () => {
    let [show,setShow]=useState(true)
  return (
    <div>
      {show?<p>error has occured</p>:null}
      <button onClick={()=>{
        setShow(!show)
      }}>toogle</button>
      
    </div>
  )
}

export default HideAndShow
