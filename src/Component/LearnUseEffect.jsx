import React, { useEffect, useState } from "react";
const LearnUseEffect = () => {
  let [count1, setCount1] = useState(0);
  let [count2, setCount2] = useState(100);
  let [count3, setCount3] = useState(200);
  console.log("first fist");
  useEffect(() => {
    console.log("nitan");
  }, [count1, count2]);
  useEffect(() => {
    console.log("ram");
  });
  console.log("second");
  return (
    <div>
      LearnUseEffect
      <br></br>
      count1 is {count1}
      <br></br>
      count2 is {count2}
      <br></br>
      count3 is {count3}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        increase count1
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        increase count2
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount3(count3 + 1);
        }}
      >
        increase count3
      </button>
    </div>
  );
};
export default LearnUseEffect;
//when does useEffect function run?
// if dependency exist
//at first render --> the function will run
//from second render --> function will run if  any dependency changes
//if dependency does not exist
//for every render the function will run








